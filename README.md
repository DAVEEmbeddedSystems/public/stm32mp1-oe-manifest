# oe-manifest
This project is the repo manifest of OpenSTLinux release.
# STM32MP1-Ecosystem-v4.1.0 release TAG: openstlinux-5.15-yocto-kirkstone-mp1-v22.11.23

See the wiki user guide for more information: http://wiki.st.com/stm32mpu/index.php/OpenSTLinux_distribution
